# Jiffie

## Reference Java program
```java
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.TimeZone;

public class Utility {

	
	 /**This method is used to convert the Jiffy format date to readable format
	 * @param dtVal
	 * @return
	 */
	public static long getDateFromJiffy(long dtVal)
	  {
	    long retDate = (int)(dtVal / 65536L) + 315513000l;
	    return retDate;
	  }
	 
	 /** This method is used to convert the 1980 format date time to readable format
	 * @param dtVal
	 * @return
	 */
	public static long getDateFromNonJiffy(long dtVal)
	 {
		 long retDate = (dtVal + 315513000l);
		 return retDate;
	 }
	 
	 /** This method is used to convert the 1970 format date time to readable format
	 * @param dtVal
	 * @return
	 */
	public static long getDateFromNonJiffy1(long dtVal)
	 {
		 return dtVal;
	 }

	 public static void main(String[] args){
		 long dateTime = getDateFromJiffy(77173842861414l);
		 Date newDate = new Date(dateTime * 1000);
		 SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:MM:ss");
		 simpleDateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		 String date = simpleDateFormat.format(newDate);
		 System.out.println("date from jiffy format:=========>"+date);
		 
		 long dateTime1 = getDateFromNonJiffy(1177579384l);
		 Date newDate1 = new Date(dateTime1 * 1000);
		 String date1 = simpleDateFormat.format(newDate1);
		 System.out.println("date from 1980 format:=========>"+date1);
		 
		 long dateTime2 = getDateFromNonJiffy1(1177579384l);
		 Date newDate2 = new Date(dateTime2);
		 String date2 = simpleDateFormat.format(newDate2);
		 System.out.println("date from 1970 format:=========>"+date2);
	 }
}
```

