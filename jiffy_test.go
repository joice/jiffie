package jiffie

import (
	"fmt"
	"testing"
	"time"
)

func TestTime(t *testing.T) {

	ji := Jiffy(time.Now())
	ti := Time(ji)
	fmt.Println(ti.Unix())
	fmt.Println(ti.Local())

}
