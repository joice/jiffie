package jiffie

// Jiffie is a tool to convert Jiffy timestamp given by National Stock Exchange or NSE into Unix Timestamp.
import (
	"time"
)

var (
	// offset to add to jiffy time - starting from 1980
	// 315513000 is the offset given by NSE. which dates exactly to 1979-12-31 18:30:00 +0000
	offset int64 = 315532800 // Calculated manually.

)

// Time converts jiffy time in int format to time.Time format.
func Time(jiffy int64) time.Time {

	// Divide jiffy time with 2^16 and divide by offset.
	epoch := (jiffy / 65536) + offset
	return time.Unix(epoch, 0)
}
